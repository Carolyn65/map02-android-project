<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

//// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$dbName = 'cp4898_conventionapp';
DB::$user = 'cp4898_convapp';
DB::$password = 'TPdOeamk&lCv';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

function error_handler($params) {
    global $log;
    if (isset($params['query'])) {
        $log->error("SQL Error: " . $params['error']);
        $log->error("SQL Query: " . $params['query']);
    } else {
        $log->error("Database Error: " . $params['error']);
    }
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error');
    die; // don't want to keep going if a query broke
}
// returns FALSE if authentication is missing or invalid
// returns users table record of authenticated users if user/pass was correct
function getAuthUser() {
    global $log;

    // WORKAROUND FOR POSSIBLE SERVER SETTINGS INCOMPATIBILITY
    list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = 
       explode(':', base64_decode(substr(getallheaders()['X-Authorization'], 6)));
    
//    $log->debug("SERVER: " . print_r($_SERVER, true));
//    $log->debug("HEADERS: " . print_r(getallheaders(), true));
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->debug(json_encode($_SERVER['PHP_AUTH_USER']));
        return FALSE;
    }
    $email = $_SERVER['PHP_AUTH_USER'];
    $log->debug($_SERVER['PHP_AUTH_USER']);
    
    $password = $_SERVER['PHP_AUTH_PW'];
    $log->debug($_SERVER['PHP_AUTH_PW']);
    
    $authUser = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    $log->debug("$authUser: " . print_r($authUser, true));
    if (!$authUser) {
        return FALSE;
    }
    if ($authUser['password'] == $password) {
        unset($authUser['password']);
        return $authUser;
    } else {
        return FALSE;
    }
}

// returns TRUE if data is valid, otherwise string describing the problem
function isUserValid($user) {
    $criteria=0;//added for pw validation
    if (is_null($user)) return "JSON parsing failed, user is null";
    if (count($user) != 4) return "Invalid number of values received";
    // remove the id, easier to do it here than convince Retrofit not to send it
    if (isset($user['id'])) unset($user['id']);
    if (filter_var($user['email'], FILTER_VALIDATE_EMAIL) === FALSE) return "Email is invalid";
    // TODO: require quality passwords, e.g. one upper-case, one lower-case, one digit or special character
    if (strlen($user['password']) < 8) return "Password too short, must be 8 characters minimum"; 
    //added password criteria validation 
    $uppercase = preg_match('@[A-Z]@', $user_password);
    if ($uppercase=True){$criteria=$criteria+1;}
    $lowercase = preg_match('@[a-z]@', $user_password);
    if ($lowercase=True){$criteria=$criteria+1;}
    $number= preg_match('@[0-9]@', $user_password);
    if ($number=True){$criteria=$criteria+1;}
    $specialChars = preg_match('@[^\w]@', $user_password);
    if ($specialChars=True){$criteria=$criteria+1;}
    
    if ($criteria<3){
        return "Password Criteria not met. 3 of the following 4 must be met. Uppercase, Lowercase, Number & special character.";
    }
    
    return TRUE;
}

// returns TRUE if data is valid, otherwise string describing the problem
function isScheduleValid($schedule) {
    global $log;
    $log->error("data recieved at validation", $schedule);
    if (is_null($schedule)) return "JSON parsing failed, todo is null";
    if (count($schedule) != 3) return "Invalid number of values received";
    $checkduplicate = DB::queryFirstField("SELECT COUNT(*) FROM schedule WHERE user_id=%i AND panel_id=%i", $schedule['user_id'], $schedule['panel_id']);
    if($checkduplicate>0) return "schedule entry already exists";
    // remove the id, easier to do it here than convince Retrofit not to send it
    if (isset($schedule['id'])) unset($schedule['id']);
  
    //line to validate panel id 
    return TRUE;
}
$app = new \Slim\Slim();
$app->response()->header('content-type', 'application/json');

$app->error(function (\Exception $e) use ($app, $log) {
    $log->error($e);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error (exception)');
});

$app->notFound(function() use ($app){
    $app->response()->status(404);
    echo json_encode("404 - not found");
});

$app->get('/test', function() use ($app) {
    $app->response()->header('content-type', 'text/html');
    phpinfo(); 
});

//Full user table get - FOR TESTING ONLY, NOT IN REAL SYSTEM
$app->get('/users', function() use ($app) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $list = DB::query("SELECT * FROM users");
    echo json_encode($list, JSON_PRETTY_PRINT);
});

//update user info 
$app->put('/users/:email', function($email) use ($log, $app) {
    // FIXME: verify todo data is valid
    // FIXME: fail with 400 if record does not exist
    // FIXME: fail if email is already used with another account
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        $log->error(sprintf("PUT /users/%s failed 401 auth missing or invalid, $email"));
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify todo data is valid   
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        $log->error(sprintf("PUT /users/%s failed 400 %s", $email, $result));
        return;
    }
    
    $hasOldUser = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE id=%i AND email=%s", $authUser['id'], $email);
    if (!$hasOldUser) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('users', $data, 'id=%s', $authUser['id']);
    echo json_encode(true);
});

//removes item from schedule
$app->delete('/schedule/:id', function($id) use ($app, $log) {
    $authUser = getAuthUser();
    
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        $log->error(sprintf("delete /Schedule/%s failed 401 %s", $email, $result));
        return;
    }
    DB::delete('schedule', 'id=%i AND user_id=%i', $id, $authUser['id']);
    echo json_encode(DB::affectedRows() != 0);
});

//insert schedule entry
$app->post('/schedule/personal', function() use ($app, $log){
    
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    $log->error("data before send to is schedule valid failed 401 %s", $data);
    
    $result = isScheduleValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }

    unset($data['id']);
    $data['user_id'] = $authUser['id'];
    DB::insert('schedule', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    $log->debug("schedule created with id=" . $id);
    echo json_encode($id);
});


//NOTE::: EVERYTHING AFTER  this point is tested and functional
//
//
//
//Pulls detail view for the Users schedule view. 
$app->get('/scheduleview/:id/',function($id) use ($app){
  
    $results = DB::queryFirstRow("SELECT panels.name, panels.id, rooms.description AS room_name, rooms.gpsLoc, panels.dateTime FROM panels, rooms WHERE panels.id=%i",$id);
    echo json_encode($results, JSON_PRETTY_PRINT);
            
});
////get users personal data
$app->get('/users/:email', function($email) use ($app, $log) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        $log->debug("GET /users/:email returned 401");
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
//    echo json_encode($user, JSON_PRETTY_PRINT);//remove me for authentication
    // only allow to view one's own record
    $log->debug("user: " . print_r($user, true));
    $log->debug("auth: " . print_r($authUser, true));
    if ($user['id'] == $authUser['id']) {
        unset($user['password']); // do NOT send password back
        $log->debug("GET /users/:email user returned:" + json_encode($user));
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        $log->debug("GET /users/:email returned 404");
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});
////inserts user for registration
$app->post('/users', function() use ($app, $log) { // NO AUTHENTICATION REQUIRED
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify user data is valid
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        $log->error(sprintf("POST /users failed 400 %s", $result));
        $log->error(" >>" + $json + "<<");
        return;
    }
    // make sure user id is not already in use
    $isUserIdInUse = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE email=%s", $data['email']);
    if ($isUserIdInUse) {
        echo json_encode("400 - User id already in use");
        $app->response()->status(400);
        $log->error(sprintf("POST /users failed 400 %s", $result));
        $log->error(" >>" + $json + "<<");
        return;
    }
    DB::insert('users', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});
//pull user personal scheudle with id expects an array with the User_id & Panel_id 
$app->get('/schedule/personal/:id', function($id) use ($app) {
    $results = DB::query("select * FROM schedule WHERE user_id=%i", $id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});

//pull schdule based on room id 
$app->get('/schedule/room/:id', function($id)use ($app) {
    $results = DB::queryFirstRow("select * FROM panels WHERE room=%i", $id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});

//pull schedule based on presenter

$app->get('/schedule/presenter/:id', function($id)use ($app) {
    $results = DB::select("select * FROM panels WHERE room=%i", $id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});

//pull presenters, except for their pictures
$app->get('/presenters', function()use ($app) {
    $results = DB::query("select id,name,bio FROM presenters");
    echo json_encode($results, JSON_PRETTY_PRINT);
});
//pull presenter, except for their pictures
$app->get('/presenters/:id', function($id)use ($app) {
    $results = DB::queryFirstRow("select id,name,bio FROM presenters WHERE id=%i", $id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});
//pull presenters picture
$app->get('/presenters/:id/picture', function($id) use ($app) {
    $binaryPicture = DB::queryFirstField("select picture FROM presenters WHERE id=%i", $id);
    $app->response()->header('content-type', 'image/jpeg');
    echo $binaryPicture;
});
//pull Schedule order by date/time (Friday /Sat /Sun to come later)  
$app->get('/panels', function()use ($app) {
    $results = DB::query("SELECT * FROM panels ORDER BY dateTime ASC");
    //$results = DB::queryFirstRow("SELECT panels.id, panels.name, panels.presenter, panels.description,panels.room, rooms.description AS roomDescription, panels.dateTime, panels.panelRating FROM panels, rooms WHERE panels.id=%i AND rooms.id=panels.room",$id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});
// pull panel based on id 
$app->get('/panels/:id', function($id)use ($app) {
    $results = DB::queryFirstRow("select * FROM panels WHERE room=%i", $id);
//    $results = DB::queryFirstRow("SELECT panels.id, panels.name, panels.presenter, panels.description,panels.room, rooms.description AS roomDescription, panels.dateTime, panels.panelRating FROM panels, rooms WHERE panels.id=%i AND rooms.id=panels.room",$id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});

//pull panel based on id with a join - Panel details - presenter name - room description & gps location 
$app->get('/panels/:id/fulldetails',function($id) use ($app){
  
    $results = DB::queryFirstRow("SELECT panels.name, presenters.name AS presenter, panels.description, panels.dateTime, panels.panelRating, rooms.description AS Room, rooms.gpsLoc FROM panels, presenters, rooms WHERE panels.id=%i",$id);
    echo json_encode($results, JSON_PRETTY_PRINT);
            
});
//pull room with id
$app->get('/rooms/:id', function($id)use ($app) {
    $results = DB::queryFirstRow("select * FROM rooms WHERE id=%i", $id);
    echo json_encode($results, JSON_PRETTY_PRINT);
});

//pull full rooms list
$app->get('/rooms', function()use ($app) {
    $results = DB::query("select * FROM rooms");
    echo json_encode($results, JSON_PRETTY_PRINT);
});

$app->run();
