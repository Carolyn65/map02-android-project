
User functions 

Full user table get /users2
Get registered users personal data /users2/:user_id
update users personal data Put /users2/:user_id
Insert new user - post - /users2

Schedule functions 
Delete entry for logged in user - delete -  /schedule/id

TESTED functions 

get users personal schedule /scheduole/personal/:id
insert schedule entry - /schedule/personal
insert schedule entry - no auth - /schedule/personal

pull schedule based on room id /scheudle/room/:id
pull schedule based on presenter /schedule/presenter/id
pull All presenters except picture /presenters/:id
pull presenters picture /presenters/:id/picture

pull Full schedule ordered by date/time /panels 

pull panel based on id /panels/:id
pull panel with join on room & presenter tables /panels/:id/fulldetails

pull room by id /rooms/:id
pull full rooms list /rooms
