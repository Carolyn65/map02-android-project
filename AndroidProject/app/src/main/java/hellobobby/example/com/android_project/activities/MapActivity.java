package hellobobby.example.com.android_project.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import hellobobby.example.com.android_project.R;

public class MapActivity extends AppCompatActivity {

    ImageView ivmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ivmap=(ImageView)findViewById(R.id.iv_Map);

        Intent intent = getIntent();
        int index = intent.getIntExtra(MainLoginActivity.EXTRA_ROOM,-1 );

        if (index == 1){
            ivmap.setImageResource(R.drawable.rm517b);
            Toast.makeText(getApplicationContext(), "1", Toast.LENGTH_SHORT).show();
        }
        else if (index == 2){
            ivmap.setImageResource(R.drawable.rm512b);
            Toast.makeText(getApplicationContext(), "2", Toast.LENGTH_SHORT).show();
        }
        else if (index == 3){
            ivmap.setImageResource(R.drawable.rm220a);
            Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
