package hellobobby.example.com.android_project.services;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import hellobobby.example.com.android_project.activities.MainLoginActivity;
import hellobobby.example.com.android_project.activities.PanelListActivity;
import hellobobby.example.com.android_project.activities.UserScheduleActivity;
import hellobobby.example.com.android_project.entities.Schedule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InitData {
    public static void initUserSchedule(final Context context){

        //Getting Panels from database
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Globals.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final ConventionAPI ConventionAPI = retrofit.create(ConventionAPI.class);
        Call<List<Schedule>> call = ConventionAPI.getPersonalSchedule(Globals.user.getId());
        call.enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    List<Schedule> list = response.body();
                    for (Schedule schedule : list) {
                        Globals.userSchedulePanels.add(PanelListActivity.panels.get(schedule.getPanel_id()-1));
                    }
                    //UserScheduleActivity.panelArrayAdapter.notifyDataSetChanged();
                } else { // we end up here when authentication fails
                    if (response.code() == 401) {
                        Toast.makeText(context, "Authentication failure", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Fetching panels failed (1)", Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
                Toast.makeText(context, "Fetching users failed (2)", Toast.LENGTH_LONG).show();
            }
        });
    }

}

