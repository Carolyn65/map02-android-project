package hellobobby.example.com.android_project.services;

import android.app.Notification;
import android.app.NotificationManager;//sends the notification to the user and it will take notification as the 2nd argument. First argument to notify function is an identifier for the notification which should be unique
import android.app.PendingIntent;
import android.content.BroadcastReceiver;//listens to broadcasts with action android.media.action.Display_Notfication this is declared in android manifest
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;// this builder is used to build a notification object which can be used by notification manager to notify the user. Notification object can have an icon, text, and pending intent which is used on click
import android.support.v4.app.TaskStackBuilder;

import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.activities.NotificationActivity;

public class AlarmManager extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent notificationIntent = new Intent(context, NotificationActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(NotificationActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            Notification notification = builder.setContentTitle("Demo App Notification")
                    .setContentText("New Notification From Demo App..")
                    .setTicker("New Message Alert!")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
}
