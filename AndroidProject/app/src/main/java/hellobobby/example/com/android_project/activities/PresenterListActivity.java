package hellobobby.example.com.android_project.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import hellobobby.example.com.android_project.entities.Presenter;
import hellobobby.example.com.android_project.R;

public class PresenterListActivity extends AppCompatActivity {
    TextView presenterStr;
    GridView gridView;
    ArrayAdapter<Presenter> presenterArrayAdapter;
    //
    TextView name;
    TextView panelDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presenter_list);
        //
        presenterStr = (TextView) findViewById(R.id.presenterStr0);
        gridView = (GridView) findViewById(R.id.presenterGridView);
        //
        Intent intent = getIntent();
        final ArrayList<Presenter> presenters = (ArrayList<Presenter>) intent.getSerializableExtra("presenters");
        // CUSTOM ARRAY ADAPTER
        presenterArrayAdapter = new PresenterListActivity.PresenterArrayAdapter(this, presenters);
        gridView.setAdapter(presenterArrayAdapter);
        gridView.setClickable(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Presenter presenter = presenters.get(position);
                Intent intent = new Intent(getApplicationContext(), PresenterPageActivity.class);
                intent.putExtra("presenter", presenter);
                startActivity(intent);
            }
        });
    }
    //ARRAY ADAPTER
    public class PresenterArrayAdapter extends ArrayAdapter<Presenter> {
        private Context context;
        private ArrayList<Presenter> presenters;
        public PresenterArrayAdapter(Context context, ArrayList<Presenter> list) {
            super(context, R.layout.panel_layout, list);
            this.context = context;
            this.presenters = list;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.panel_layout, parent, false);
            name = (TextView) rowView.findViewById(R.id.tv_panelName);
            TextView description = (TextView) rowView.findViewById(R.id.tv_panelDescription);
            ImageView panelImage = (ImageView) rowView.findViewById(R.id.iv_panelPageImage);
            //
            Presenter presenter = presenters.get(position);
            name.setText(presenter.getName());
            description.setText(presenter.getBio());
            //
            return rowView;
        }
    }
}
