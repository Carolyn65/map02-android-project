package hellobobby.example.com.android_project.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hellobobby.example.com.android_project.entities.Presenter;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.services.Globals;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterPageActivity extends AppCompatActivity {
    ImageView presenter_image;
    TextView name;
    TextView bio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presenter_page);
        //
        Intent intent = getIntent();
        Presenter presenter = (Presenter)intent.getSerializableExtra("presenter");
        //
        name = (TextView) findViewById(R.id.presenter_name);
        bio = (TextView) findViewById(R.id.presenter_bio);
        presenter_image = findViewById(R.id.presenter_image);
        //
        name.setText(presenter.getName());
        bio.setText(presenter.getBio());
        Call<ResponseBody> call = Globals.conventionAPI.getPresenterPhoto(presenter.getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        ResponseBody rb = response.body();
                        byte[] blobAsBytes = rb.bytes();
                        Bitmap btm = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobAsBytes.length);
                        presenter_image.setImageBitmap(btm);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("TAG", "Error fetching Image 1");
                    Toast.makeText(getApplicationContext(), "Error fetching Image 1", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("TAG", "Error fetching Image 2", t);
                Toast.makeText(getApplicationContext(), "Error fetching Image 2", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
