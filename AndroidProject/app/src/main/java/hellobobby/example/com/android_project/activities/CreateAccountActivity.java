package hellobobby.example.com.android_project.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hellobobby.example.com.android_project.entities.User;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.services.Globals;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CreateAccountActivity extends AppCompatActivity {

    EditText et_email;
    EditText et_password;
    EditText et_passwordConfirm;
    EditText et_nickname;
    Retrofit retrofit_3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        //
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        et_passwordConfirm = (EditText) findViewById(R.id.et_passwordConfirm);
        et_nickname = (EditText) findViewById(R.id.et_nickname);

        button = (Button) findViewById(R.id.bt_createAccount);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performRegistration();
                finish();
            }
        });
    }
    public void performRegistration(){
        String name = et_nickname.getText().toString();
        if (et_password.getText().toString().equals(et_passwordConfirm.getText().toString())){
            String password = et_password.getText().toString();

            User user = new User();
            user.setEmail(et_email.getText().toString());
            user.setPassword(password);
            user.setName(name);
            //TODO: Pass the user
            //

            Call<Integer> callAddUser = Globals.conventionAPI.addUser(user);
            callAddUser.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    if (response.isSuccessful()) {
                        int id = response.body().intValue();
                        Toast.makeText(getApplicationContext(), "User added with id " + id, Toast.LENGTH_LONG).show();
                    } else {
                        try {
                            String error = response.errorBody().toString();
                            Toast.makeText(getApplicationContext(), "Adding user failed (1)\n" + error, Toast.LENGTH_LONG).show();
                        } catch (NullPointerException ex) {
                            Toast.makeText(getApplicationContext(), "Adding user failed (3-NullPointer)", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Adding user failed (2)", Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(), "Passwords don't match", Toast.LENGTH_SHORT).show();
        }
    }
}
