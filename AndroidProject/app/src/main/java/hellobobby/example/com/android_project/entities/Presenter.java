package hellobobby.example.com.android_project.entities;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Presenter implements Serializable {

    private int id;
    private String name;
    private Bitmap picture;
    private String bio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }
    public void setBio(String bio) {
        this.bio = bio;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }
}
