package hellobobby.example.com.android_project.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import hellobobby.example.com.android_project.entities.Panel;
import hellobobby.example.com.android_project.entities.Presenter;
import hellobobby.example.com.android_project.R;
import hellobobby.example.com.android_project.services.Globals;
import hellobobby.example.com.android_project.services.ConventionAPI;
import hellobobby.example.com.android_project.services.InitData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PanelListActivity extends AppCompatActivity {

    public static ArrayList<Panel> panels;

    ArrayAdapter<Panel> panelArrayAdapter;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel_list);
        //
        gridView = (GridView) findViewById(R.id.gv_panels);
        panels = new ArrayList<>();

        //Getting Panels from database
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Globals.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        final ConventionAPI ConventionAPI = retrofit.create(ConventionAPI.class);
        Call<List<Panel>> call = ConventionAPI.getPanels();
        call.enqueue(new Callback<List<Panel>>() {
            @Override
            public void onResponse(Call<List<Panel>> call, Response<List<Panel>> response) {
                if (response.isSuccessful()) {
                    List<Panel> list = response.body();

                    for (final Panel panel : list) {
                        Log.d("tag", "Panel: " + panel);
                        //FIXME: </3 :'(
                        Call<Presenter> call_3 = ConventionAPI.getPresenterById(panel.getPresenter());
                        call_3.enqueue(new Callback<Presenter>() {
                            @Override
                            public void onResponse(Call<Presenter> call, Response<Presenter> response) {
                                Presenter presenter = response.body();
                                panel.setPresenterObject(presenter);
                            }

                            @Override
                            public void onFailure(Call<Presenter> call, Throwable t) {

                            }
                        });
                        panels.add(panel);
                    }
                    InitData.initUserSchedule(getApplicationContext());
                    panelArrayAdapter.notifyDataSetChanged();
                } else { // we end up here when authentication fails
                    if (response.code() == 401) {
                        Toast.makeText(getApplicationContext(), "Authentication failure", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Fetching panels failed (1)", Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<List<Panel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetching users failed (2)", Toast.LENGTH_LONG).show();
            }
        });
        // CUSTOM ARRAY ADAPTER
        panelArrayAdapter = new PanelArrayAdapter(this, panels);
        gridView.setAdapter(panelArrayAdapter);
        gridView.setClickable(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Panel panel = panels.get(position);
                Intent intent = new Intent(getApplicationContext(), PanelPageActivity.class);
                intent.putExtra("panel", panel);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.panel_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            //TODO: add on clicks here
            case R.id.mi_mySchedule:
                Intent intent = new Intent(getApplicationContext(), UserScheduleActivity.class);
                intent.putExtra("panels", panels);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //ARRAY ADAPTER
    public class PanelArrayAdapter extends ArrayAdapter<Panel> {
        private final static String TAG = "PanelArrayAdapter";
        private Context context;
        private List<Panel> list;
        public PanelArrayAdapter(Context context, List<Panel> list) {
            super(context, R.layout.panel_layout, list);
            this.context = context;
            this.list = list;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.panel_layout, parent, false);
            TextView tv_panelName = rowView.findViewById(R.id.tv_panelName);
            TextView tv_panelDescription = rowView.findViewById(R.id.tv_panelDescription);
            final ImageView iv_panelImage = rowView.findViewById(R.id.iv_panelPageImage);
            tv_panelName.setText(list.get(position).getName());
            tv_panelDescription.setText(list.get(position).getDescription());
            final Panel panel = list.get(position);

            Call<ResponseBody> call = Globals.conventionAPI.getPresenterPhoto(panel.getPresenter());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            ResponseBody rb = response.body();
                            byte[] blobAsBytes = rb.bytes();
                            Bitmap btm = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobAsBytes.length);
                            iv_panelImage.setImageBitmap(btm);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "Error fetching Image 1");
                        Toast.makeText(PanelListActivity.this, "Error fetching Image 1", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Error fetching Image 2", t);
                    Toast.makeText(PanelListActivity.this, "Error fetching Image 2", Toast.LENGTH_SHORT).show();

                }
            });
            return rowView;
        }
    }
}
